import express, { Request, Response } from "express";
import getMember from "../services/getMember";
import getMembers from "../services/getMembers";
import addMember from "../services/addMember";
import cors from "cors";

const app = express();
const port = process.env.PORT || 3000;

const host = process.env.BE_HOST || '0.0.0.0';
const FE_HOST = process.env.FE_HOST || '0.0.0.0';
const FE_PORT = process.env.FE_PORT || '8080';

const allowedOrigins = [
    `http://${FE_HOST}:${FE_PORT}`,
    `http://${host}:${port}`,
    `http://localhost:${port}`,
    `http://localhost:${FE_PORT}`,
    `http://127.0.0.1:${port}`,
    `http://127.0.0.1:${FE_PORT}`,
];

const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors(options));
app.use(express.json());

app.get("/", (_req: Request, res: Response) => {
  res.send("--Contour Test API--");
});

app.get("/members/:id", getMember);

app.get("/members", getMembers);

app.post("/members", addMember);

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
