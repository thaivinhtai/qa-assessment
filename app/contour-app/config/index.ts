const HOST = process.env.NEXT_PUBLIC_BE_HOST || '0.0.0.0';
const API_PORT = process.env.NEXT_PUBLIC_BE_PORT || '3000';

export const ApiServer = `http://${HOST}:${API_PORT}`;

export const EndPoints = {
  GetMembers: `${ApiServer}/members`,
  GetMember: `${ApiServer}/members/:id`,
  AddMember: `${ApiServer}/members`,
};
