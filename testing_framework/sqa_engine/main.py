#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Framework executor.
"""

from sys import path
from pathlib import Path
from hanging_threads import start_monitoring


def main() -> None:
    """Main Function"""

    # Import custom library
    from sqa_engine.utilities.workspace_vars import \
        set_version_and_environment_env_var, announce_debug_is_enable
    from sqa_engine.utilities.args_handler import ARGUMENTS
    from sqa_engine.utilities.executor import execute_test_cases, push_result
    from sqa_engine.utilities.framework_generator import (
        generate_sequence, generate_docs_folder, generate_suite_files_structure
    )
    from sqa_engine.utilities.test_suites_management import get_list_test_suite

    # Export Version for dynamic import locator and data
    set_version_and_environment_env_var(ARGUMENTS.version,
                                        ARGUMENTS.environment)

    # trace hanging threads
    monitoring_thread = None
    if ARGUMENTS.trace_thread:
        # Check if a thread is stopping 30s and more
        monitoring_thread = start_monitoring(seconds_frozen=30,
                                             test_interval=100)
    # Set debugging if any
    announce_debug_is_enable(ARGUMENTS.debug)

    # Get list test suites
    ARGUMENTS.test_designs = get_list_test_suite(ARGUMENTS.module,
                                                 ARGUMENTS.test_designs)

    if ARGUMENTS.create_suite:
        generate_suite_files_structure(ARGUMENTS.module,
                                       ARGUMENTS.create_suite)
        return

    # Generate documents and sign the sequence of test cases to be executed
    if ARGUMENTS.gen_scripts:
        for suite in ARGUMENTS.test_designs:
            generate_docs_folder(module_name=ARGUMENTS.module,
                                 test_design=suite,
                                 version=ARGUMENTS.version)
            generate_sequence(test_module=ARGUMENTS.module, test_design=suite)

    # Run test cases in normal environment
    execute_test_cases(ARGUMENTS)

    if ARGUMENTS.push_result:
        # Run test cases in headless mode for CI
        if ARGUMENTS.gen_scripts:
            pass
        elif ARGUMENTS.debug or ARGUMENTS.stop_on_failure:
            raise Exception("Can not push result to server when debug is on"
                            " or stop-on-failure is enable.")
        push_result(ARGUMENTS.push_result)

    if ARGUMENTS.trace_thread:
        monitoring_thread.stop()


if __name__ == "__main__":
    # Add workspace to python path, mark it as an Python lib
    path.append(str(Path(__file__).parent.absolute().parent.parent))
    # Add sqa_engine to python path
    path.append(str(Path(__file__).parent.absolute().parent))
    # run main engine
    main()
