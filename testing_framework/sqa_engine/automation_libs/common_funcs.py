#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""


from getpass import getuser
from json import loads, dumps
from typing import Union

from behave.contrib.scenario_autoretry import patch_scenario_with_autoretry
from behave.model import ScenarioOutline, Scenario

from string import ascii_lowercase, ascii_uppercase, digits, punctuation, \
    whitespace
from random import choice, randint, uniform
from allure import attach
from allure_commons.types import AttachmentType
import allure

from sqa_engine.utilities.logging.custom_logger import LOGGER

from sqa_engine.utilities.workspace_vars import InternalPath
from sqa_engine.utilities.args_handler import ARGUMENTS


ALLURE_ATTACHMENT_TYPE = {
    'txt': AttachmentType.TEXT,
    'csv': AttachmentType.CSV,
    'tsv': AttachmentType.TSV,
    'uri': AttachmentType.URI_LIST,
    "html": AttachmentType.HTML,
    "xml": AttachmentType.XML,
    "json": AttachmentType.JSON,
    "yaml": AttachmentType.YAML,
    "yml": AttachmentType.YAML,
    "pcap": AttachmentType.PCAP,
    "png": AttachmentType.PNG,
    "jpg": AttachmentType.JPG,
    "svg": AttachmentType.SVG,
    "gif": AttachmentType.GIF,
    "bmp": AttachmentType.BMP,
    "tiff": AttachmentType.TIFF,
    "mp4": AttachmentType.MP4,
    "ogg": AttachmentType.OGG,
    "webm": AttachmentType.WEBM,
    "pdf": AttachmentType.PDF,
}
SCENARIO_DESCRIPTION = ""
SCENARIO_NAME = ""
SCENARIO_TAGS = ""


def log_current_executor() -> None:
    """Log current executor's info.

    This function helps to log current tester, who executes the test case.

    Returns
    -------
    None
    """
    current_user = getuser()
    current_dir = InternalPath.workspace_path
    LOGGER.info("", timestamp=False)
    LOGGER.info(f'Test case ' +
                f'is conducted by: {current_user}')
    LOGGER.info(f'Workspace locates at {current_dir}')
    LOGGER.info(f'========== Test Case Begins ==========')


def random_str(length: int, have_punctuation: bool = True,
               have_space: bool = False, have_digits: bool = True) -> str:
    """Generate random string.

    This function generates random string with given length.

    Parameters
    ----------
    have_digits
    have_space
    have_punctuation
    length : int
        Length of string to be generated.

    Returns
    -------
    string
        A random string.
    """
    letters = ascii_lowercase + ascii_uppercase
    if have_digits:
        letters += digits
    if have_punctuation:
        letters += punctuation
    if have_space:
        letters += whitespace
    return ''.join(choice(letters) for i in range(length))


def random_number(min_number=0, max_number=0) -> int:
    """Generate random number.

    This function generates a random min_number from 0 to max_number.

    Parameters
    ----------
    min_number : int
        The smallest number can be generated.
    max_number : int
        The largest number can be generated.

    Returns
    -------
    int
        A random number.
    """
    return randint(min_number, max_number)


def random_float_number(min_number: float = 0.0, max_number: float = 0.0,
                        ignore_0: bool = False) -> float:
    """Generate random number.

    This function generates a random number from min_number to max_number.

    Parameters
    ----------
    min_number : float
        The smallest number can be generated.
    max_number : float
        The largest number can be generated.
    ignore_0 : bool
        True will not return 0

    Returns
    -------
    float
        A random number.
    """
    result = uniform(min_number, max_number)
    if ignore_0:
        while result == 0.0:
            result = uniform(min_number, max_number)
    return uniform(min_number, max_number)


def attach_file_to_report(attachments: list) -> None:
    """Attach logfile in text to Allure report.

    Returns
    -------
    None
    """
    for attachment in attachments:
        try:
            attachment_file = open(attachment, 'rb')
            file_type = attachment.split("/")[-1].split('.')[-1]
            attach_file = attachment_file.read()
            attach(attach_file, name='attachment',
                   attachment_type=ALLURE_ATTACHMENT_TYPE.get(file_type))
            attachment_file.close()
        except FileNotFoundError as ex:
            print('Could not attach log file.', ex)


def update_dictionary(dict_1: dict, json_str: str) -> dict:
    """Update dictionary

    Parameters
    ----------
    dict_1 : dict
        first dict
    json_str : str
        a Json string

    Returns
    -------
    dict
        merge dict_1 and json
    """
    dict_2 = loads(json_str)
    dict_1.update(dict_2)
    return dict_1


def update_body_content(dict_1: dict, json_str: str) -> dict:
    """Update dictionary

    Parameters
    ----------
    dict_1 : dict
        first dict
    json_str : str
        a Json string

    Returns
    -------
    dict
        merge dict_1 and json
    """
    dict_2 = loads(json_str)
    for key, value in dict_2.items():
        if isinstance(value, str):
            if value.lower() == 'random':
                dict_1[key] = random_str(random_number(1, 100))
            elif 'random_str_with_length' in value.lower():
                length = value.lower().split('_')[-1]
                dict_1[key] = random_str(int(length))
            # elif value.lower().find('random_float') >= 0:
            elif 'random_float' in value.lower():
                ignore_0 = True if 'ignore_0' in value.lower() else False
                components = value.lower().split('_')
                if len(components) == 4:
                    dict_1[key] = random_float_number(float(components[2]),
                                                      float(components[3]),
                                                      ignore_0)
                else:
                    dict_1[key] = random_float_number(-100, 100, ignore_0)
                # dict_1[key] = random_float_number(-100, 100, ignore_0)
            elif "random_int" in value.lower():
                components = value.lower().split('_')
                if len(components) == 4:
                    dict_1[key] = random_number(int(components[2]),
                                                int(components[3]))
                else:
                    dict_1[key] = random_number(-1000, 1000)
            elif "random_array" in value.lower():
                components = value.lower().split('_')
                if components[-1] == "float":
                    dict_1[key] = [random_float_number(-100, 100)
                                   for _ in range(int(components[2]))]
                if components[-1] == "int":
                    dict_1[key] = [random_number(-100, 100)
                                   for _ in range(int(components[2]))]
            else:
                dict_1[key] = value
        elif isinstance(value, dict):
            temp = update_body_content(dict_1.get(key, {}), dumps(value))
            dict_1[key] = temp
        elif isinstance(value, list):
            if "replace" in value:
                value.remove('replace')
                dict_1[key] = value
                continue
            index = 0
            for i in range(len(value)):
                try:
                    if isinstance(value[i], dict):
                        if not dict_1.get(key):
                            dict_1[key] = [{}]
                        temp = update_body_content(dict_1[key][i],
                                                   dumps(value[i]))
                        dict_1[key][i] = temp
                        continue
                    dict_1[key][i] = value[i]
                except IndexError:
                    pass
                finally:
                    index += 1
            for i in range(index, len(value)):
                dict_1[key].append(value[i])
        else:
            dict_1[key] = dict_2[key]
    return dict_1


def update_nested(dict_1: dict, json_str: str) -> dict:
    """Update nested.

    Update nested dict with specified key - value

    Parameters
    ----------
    dict_1 : dict
        first dict
    json_str : str
        Json string

    Returns
    -------
    dict
    """

    def update(in_dict, key, value):
        for k, v in in_dict.items():
            if key == k:
                in_dict[k] = value
            elif isinstance(v, dict):
                update(v, key, value)
            elif isinstance(v, list):
                for o in v:
                    if isinstance(o, dict):
                        update(o, key, value)

    update_dict = loads(json_str)
    for key_, value_ in update_dict.items():
        update(dict_1, key_, value_)
    return dict_1


def expect_true(result: bool, failed_message: str = "", defect_code: str = "",
                ticket: str = "", instance: any = None) -> None:
    if ticket:
        allure.dynamic.link(url=ticket, link_type="link", name=ticket)
    if not result:
        if defect_code:
            allure.dynamic.issue(defect_code, defect_code)
        if instance:
            instance.driver.take_screenshot()
        raise AssertionError(failed_message if not defect_code
                             else f"Failed with defect: {defect_code}")


def expect_false(result: bool, failed_message: str = "", defect_code: str = "",
                 ticket: str = "", instance: any = None) -> None:
    if ticket:
        allure.dynamic.link(url=ticket, link_type="link", name=ticket)
    if result:
        if defect_code:
            allure.dynamic.issue(defect_code, defect_code)
        if instance:
            instance.driver.take_screenshot()
        raise AssertionError(failed_message if not defect_code
                             else f"Failed with defect: {defect_code}")


def make_beauty_behave_testcase_document_allure_report(scenario: any) -> None:
    """

    """
    global SCENARIO_DESCRIPTION
    testcase_documentation = ""
    new_line = True
    for token in scenario.description:
        if '"""' == token:
            continue
        if token.startswith("**"):
            testcase_documentation += f"\n<br>{token}"
            continue
        if token.startswith("-"):
            testcase_documentation += f"\n{token}\n"
            continue
        if token[0].isdigit() and token.find(".") > 0:
            testcase_documentation += f"\n{token}\n"
            continue
        if token.lower().startswith("level") \
                or token.lower().startswith("min_version") \
                or token.lower().startswith("max_version") \
                or token.lower().startswith("mode"):
            new_metadata = f"<br>{token} " if new_line \
                else f"- {token} "
            new_line = False
            testcase_documentation += new_metadata
            continue
        testcase_documentation += f"{token}"
    allure.dynamic.description(testcase_documentation)
    SCENARIO_DESCRIPTION = testcase_documentation


def retry_on_failure(scenario: any) -> None:
    """

    """
    patch_scenario_with_autoretry(scenario,
                                  max_attempts=int(ARGUMENTS.retry_times))


def set_default_test_case_behavior(scenario: any,
                                   tsm_link_mapping: dict = None) -> None:
    """

    """
    global SCENARIO_TAGS, SCENARIO_NAME
    make_beauty_behave_testcase_document_allure_report(scenario)
    SCENARIO_NAME = scenario.name
    SCENARIO_TAGS = str(scenario.tags) if scenario.tags else ""
    if tsm_link_mapping:
        scenario_id = SCENARIO_NAME.split("-")[0].strip().rstrip()
        allure.dynamic.link(url=tsm_link_mapping.get(scenario_id),
                            name=scenario_id)


def mapping_browser_name_with_scenario_outline(
        scenario_: Union[Scenario, ScenarioOutline], browser_name: str
) -> Union[Scenario, ScenarioOutline]:
    """

    Parameters
    ----------
    scenario_
    browser_name

    Returns
    -------

    """
    browser_name = browser_name.lower()
    browser_name = browser_name.replace(browser_name[0],
                                        browser_name[0].upper(), 1)
    if type(scenario_) == Scenario:
        return scenario_
    for example_ in scenario_.examples:
        for index in range(len(example_.table.rows)):
            example_.table.rows[index].cells[0] = browser_name
    for scenario in scenario_.scenarios:
        scenario.description = scenario_.description
    return scenario_


def mapping_browser_name_platform_with_scenario_outline(
        scenario_: Union[Scenario, ScenarioOutline], platform: str
) -> Union[Scenario, ScenarioOutline]:
    """

    Parameters
    ----------
    scenario_
    platform

    Returns
    -------

    """
    platform = platform.lower()
    platform = platform.replace(platform[0], platform[0].upper())
    if platform == "ios":
        platform = platform.replace(platform[1:], platform[1:].upper())
    if type(scenario_) == Scenario:
        return scenario_
    for example_ in scenario_.examples:
        if platform == "iOS":
            example_.table.rows[0].cells = [platform, "Safari"]
        else:
            example_.table.rows[0].cells = [platform, "Chrome"]
    for scenario in scenario_.scenarios:
        scenario.description = scenario_.description
    return scenario_


def mapping_platform_version_with_scenario_outline(
        scenario_: Union[Scenario, ScenarioOutline],
        platform: str, version: str
) -> Union[Scenario, ScenarioOutline]:
    """

    Parameters
    ----------
    scenario_
    platform
    version

    Returns
    -------

    """
    platform = platform.lower()
    if platform == "ios":
        platform = platform.replace(platform[1:], platform[1:].upper())
    else:
        platform = platform.replace(platform[0], platform[0].upper())
    if type(scenario_) == Scenario:
        return scenario_
    for example_ in scenario_.examples:
        example_.table.rows[0].cells = [platform, version]
    for scenario in scenario_.scenarios:
        scenario.description = scenario_.description
    return scenario_


def set_story_suite_for_desktop_web(browser_name: str, suite: str,
                                    parent_suite: str = "Desktop Web") -> None:
    """

    """
    browser_name = browser_name.lower()
    browser_name = browser_name.replace(browser_name[0],
                                        browser_name[0].upper(), 1)
    allure.dynamic.story(browser_name)
    allure.dynamic.parent_suite(parent_suite)
    allure.dynamic.suite(suite)
    allure.dynamic.sub_suite(browser_name)
