#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains function for convenience.
"""

from requests import request
from gzip import decompress
from http import client
from ssl import SSLContext, PROTOCOL_SSLv23
from json import dumps, JSONEncoder, loads, JSONDecodeError
from robot.libraries.BuiltIn import DotDict

from sqa_engine.utilities.logging.custom_logger import LOGGER


class CustomJSONEncoder(JSONEncoder):
    pass


def make_beauty_json(json_data: dict) -> str:
    """Make beauty JSON.

    Transform JSON instance into readable form.

    Parameters
    ----------
    json_data : a dictionary or a JSON instance

    Returns
    -------
    str
    """
    return dumps(json_data, indent=4, sort_keys=True, cls=CustomJSONEncoder) \
        if type(json_data) in (dict, DotDict, list) else json_data


def send_request_and_receive_response(method: str, uri: str, **kwargs) -> dict:
    """Send and receive request.

    This function sends and receives request then logs to console.

    Parameters
    ----------
    method : str
        HTTP method
    uri : str
        URI

    Returns
    -------
    dict
        {"message": , "status_code": }
    """
    headers = kwargs.pop('headers', {'Accept': '*/*'})
    body = kwargs.pop('body', {})

    beauty_headers = make_beauty_json(headers)
    beauty_body = make_beauty_json(body)
    LOGGER.debug("Request Headers: \n" + beauty_headers)
    LOGGER.debug("Request data: \n" + beauty_body)

    response = request(method, uri, headers=headers,
                       data=body, **kwargs)

    print('status: ', response.headers, response.content)
    response_message = response.json()
    beauty_response = make_beauty_json(response_message)
    status_code = response.status_code

    LOGGER.debug("Got response message: \n" + str(beauty_response))
    LOGGER.info("Status code: " + str(status_code))

    return {'message': response_message, 'status_code': status_code}


def request_with_client_cert(host: str, path: str, cert_file: str,
                             key_file: str, **kwargs):
    """

    Parameters
    ----------
    host
    path
    cert_file
    key_file

    Returns
    -------
    str
    """
    headers = kwargs.pop('headers', {"Accept": "*/*"})
    body = kwargs.pop('body', "")
    method = kwargs.pop('method', 'GET')
    port = kwargs.pop('port', 443)

    # Define the client certificate settings for https connection
    context = SSLContext(PROTOCOL_SSLv23)
    LOGGER.debug(f'Load cert file and key file:')
    LOGGER.debug(f'Cert file: {cert_file}')
    LOGGER.debug(f'Key file: {key_file}')
    context.load_cert_chain(certfile=cert_file, keyfile=key_file)
    # Create a connection to submit HTTP requests
    connection = client.HTTPSConnection(host, port=port, context=context)
    # Use connection to submit a HTTP POST request
    beauty_headers = make_beauty_json(headers)
    beauty_body = make_beauty_json(body)
    LOGGER.debug("Request URI: \n" + host + path + "\n")
    LOGGER.debug("Request Headers: \n" + beauty_headers + "\n")
    LOGGER.debug("Request data: \n" + beauty_body + "\n")
    connection.request(method=method, url=path,
                       headers=headers, body=dumps(body))
    response = connection.getresponse()

    data = response.read()
    try:
        data = data.decode('utf-8')
    except UnicodeDecodeError:
        data = decompress(data).decode()

    try:
        data = loads(data)
    except JSONDecodeError:
        pass

    status_code = response.status

    LOGGER.debug("Got response message: \n" + make_beauty_json(data) + "\n")
    LOGGER.info("Status code: " + str(status_code) + "\n")

    return data, status_code
