#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Package framework_generator contains modules that relate to
document generating.
"""

from .sequence_generators import generate_sequence
from .document_dirs_generator import generate_docs_folder
from .test_suites_generator import generate_suite_files_structure
