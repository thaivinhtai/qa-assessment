#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module generate folders that stores documentation of test cases and test
Library.
"""

from datetime import datetime
from os import mkdir, path
from sqa_engine.utilities.workspace_vars import InternalPath


def generate_docs_folder(module_name: str, test_design: str,
                         version: str) -> None:
    """Generate folder for documentation.

    This function generates folder that stores documentations before generate
    document files.

    The folder structure are classified by:
        workspace/{date}/{module}/{version}/{current_time}-{test_design}

    Parameters
    ----------
    module_name : str
        Module name {mobile_app, web_portal, rest_api}
    test_design : str
        Test suite specification domain
    version : str
        Version of documentations

    Returns
    -------
    None
    """
    today = datetime.now().strftime("%Y-%m-%d")
    current_time = datetime.now().strftime("%H-%M-%S")
    doc_path = InternalPath.document_folder
    path_to_test_design = test_design.split('/')
    test_design = path_to_test_design[-1]
    path_to_test_design.remove(test_design)

    today_doc_folder = f'{doc_path}/{today}'
    module_doc_folder = f'{today_doc_folder}/{module_name}'
    version_doc_folder = f'{module_doc_folder}/{version}'
    executing_doc_folder = f'{current_time}-{test_design}'
    temp_path = version_doc_folder

    [mkdir(directory) for directory in
     (doc_path, today_doc_folder, module_doc_folder, version_doc_folder)
     if not path.exists(directory)]

    for folder in path_to_test_design:
        if not path.exists(f'{temp_path}/{folder}'):
            temp_path = f'{temp_path}/{folder}'
            mkdir(temp_path)
    if not path.exists(f'{temp_path}/{executing_doc_folder}'):
        mkdir(f'{temp_path}/{executing_doc_folder}')

    # update path to InternalPath instance attribute.
    InternalPath.current_doc_folder = f'{temp_path}/{executing_doc_folder}'
