#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

from os import mkdir, path
from re import sub

from pathlib import Path
from getpass import getuser
from datetime import datetime

from sqa_engine.utilities.test_suites_management import \
    convert_sequence_to_camel_case
from sqa_engine.utilities.workspace_vars import InternalPath


__CURRENT_DIR = str(Path(__file__).parent.absolute())
__SEQUENCE_FILE_CONTENT = f"{__CURRENT_DIR}/sequence.content"
__ROBOT_FILE_CONTENT = f"{__CURRENT_DIR}/robot.content"
__FEATURE_FILE_CONTENT = f"{__CURRENT_DIR}/feature.content"
__CURRENT_DATE = datetime.now().strftime('%d/%m/%Y')


def __convert_suite_to_normal_name(suite: str) -> str:
    """

    Parameters
    ----------
    suite

    Returns
    -------

    """
    components = suite.split('_')
    return ''.join(component.title() for component in components)


def __create_path_and_suite_needed_files(
        module_path: str, suite_path: str) -> None:
    """

    Parameters
    ----------
    module_path
    suite_path
    behave

    Returns
    -------

    """
    extension = "feature"
    # Resolve suite path and name
    temp_path = module_path
    parts_of_suite = suite_path.split('/')
    suite_name = parts_of_suite[-1]
    sequence_suite_name = convert_sequence_to_camel_case(suite_name)

    sequence_file_content = open(__SEQUENCE_FILE_CONTENT).read()
    sequence_file_content = sub(
        "{{suite_name}}", __convert_suite_to_normal_name(suite_name),
        sequence_file_content
    )
    sequence_file_content = sub(
        "{{sequence_suite_name}}", sequence_suite_name, sequence_file_content
    )

    testcase_file_content = open(__FEATURE_FILE_CONTENT).read()

    # Create suite path
    for part in parts_of_suite:
        temp_path += f'/{part}'
        if not path.exists(temp_path):
            mkdir(temp_path)

    sequence_file_name = f"{temp_path}/sequence_{suite_name}.py"
    robot_file_name = f"{temp_path}/{suite_name}.{extension}"

    testcase_file_content = sub(
        "{{suite_name}}",
        __convert_suite_to_normal_name(suite_name), testcase_file_content
    )
    testcase_file_content = sub(
        "{{current_user}}", getuser(), testcase_file_content
    )
    testcase_file_content = sub(
        "{{current_time}}", __CURRENT_DATE, testcase_file_content
    )
    __behave_suite_structure(module_path=module_path,
                             suite_path=suite_path, suite_name=suite_name)

    if path.exists(sequence_file_name) or path.exists(robot_file_name):
        return print(f'Test suite {suite_name} is already existed.')

    # Write files.
    for file, content in zip((sequence_file_name, robot_file_name),
                             (sequence_file_content, testcase_file_content)):
        with open(file, 'w') as file_object:
            file_object.writelines(content)
        file_object.close()
    return print(f'Test suite {suite_name} is created successfully.')


def __behave_suite_structure(module_path: str, suite_path: str,
                             suite_name: str) -> None:
    """

    Parameters
    ----------
    module_path
    suite_path
    suite_name

    Returns
    -------

    """
    # Create steps folder
    steps_folder = f"{module_path}/{suite_path}/steps"
    mkdir(steps_folder)
    # Create environment file
    open(f"{module_path}/{suite_path}/environment.py", "w")
    # Create init file in steps folder
    open(f"{steps_folder}/__init__.py", "w")
    # Create steps file
    open(f"{steps_folder}/{suite_name}.py", "w")


def generate_suite_files_structure(
        module_name: str, list_suites_to_be_created: list) -> None:
    """

    Parameters
    ----------
    module_name
    list_suites_to_be_created

    Returns
    -------

    """

    module_path = \
        InternalPath.test_module.get(f'{module_name}') + '/test_designs'

    for suite in list_suites_to_be_created:
        __create_path_and_suite_needed_files(module_path, suite)


if __name__ == "__main__":
    print(InternalPath.test_module)
