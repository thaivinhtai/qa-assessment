#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module gets arguments via specified flags.
"""


import argparse


def __get_args() -> argparse.Namespace:
    """Get test arguments.

    Returns
    -------
    argparse.Namespace
        module : module to be tested
        test_design : test design specification of feature to be tested
        version : Specify version
        environment : Specify the environment to be tested
        priority : {High, Medium, .. }
        tags : tag of test cases
        browser : browser to execute test on
        debug : Use when debugging, when this flag is enable the
                test cases won't re-run on failure.
        trace_thread : Use when tracing the python process hangs.
        stop_on_failure : Immediately stop the script when failure is happened
        retry_times : The amount of times to be re-run when test cases failed,
                      default = 3.
        run_allure : start allure reporting server after test.
        gen_scripts : generate sequence and test documentation.
        run_job : Push result to release ID, only use this flag for
                  running CI/CD.
        push_result : Push execution results to reporting server with
                      specified project ID.
        create_suite : Create test suite folders and files following framework
                       structure.
    """
    parser = argparse.ArgumentParser(description='Test Execution arguments '
                                                 'handling')
    parser.add_argument('-m', '--module', required=False, default='web',
                        help='{REST, mobile, web, cooperation}')
    parser.add_argument('--test-designs', nargs='*',
                        required=False, default=['all'],
                        help='The Test Design Specification name to use for '
                             'execution of tests, "all" for run all TDS')
    parser.add_argument('-v', '--version', required=False, default='1.0.0',
                        help='Specify version UI/API')
    parser.add_argument('-e', '--environment', required=False,
                        default='qa',
                        help='Specify the environment to be tested')
    parser.add_argument('-p', '--priority', required=False,
                        default=['all'], nargs='*',
                        help='[High, Medium, ..., All]')
    parser.add_argument('-t', '--tags', required=False, nargs='*',
                        help='Identifier of test cases to be run')
    parser.add_argument('--browser', required=False,
                        default='chrome', help='Browser to be run')
    parser.add_argument('--run-allure', required=False, action='store_true',
                        help='Run Allure report server in local')
    parser.add_argument('--gen-scripts', required=False, action='store_true',
                        help='Generate sequence and create test documentation')
    parser.add_argument('--push-result', required=False,
                        help='Push execution results to reporting server with '
                             'specified project ID')
    parser.add_argument('--debug', required=False, action="store_true",
                        help="Use when debugging, when this flag is enable, "
                             "the test cases won't re-run on failure.")
    parser.add_argument('--trace-thread', required=False, action="store_true",
                        help="Use when tracing the python process hangs.")
    parser.add_argument('--stop-on-failure', required=False,
                        action="store_true", help=
                        "Immediately stop the script when failure is happened")
    parser.add_argument('--retry-times', required=False, default=1,
                        help="The amount of times to be re-run when test cases"
                             "failed, default=3")
    parser.add_argument('--create-suite', required=False, nargs='*',
                        help='Create test suite folders and files following'
                             'the framework structure.')
    return parser.parse_args()


# This global variable stores value of __get_args(), this makes ArgumentParser
# be initialed just one time and store the argparse.Namespace that we can use
# later.

ARGUMENTS = __get_args()
