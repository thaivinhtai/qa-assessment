#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module handles test cases execution.
"""

from argparse import Namespace

from threading import current_thread

from sqa_engine.utilities.framework_generator.log_dirs_generator import (
    generate_test_suite_logs_folder, generate_current_time_execution_log_dir
)

from sqa_engine.utilities.test_suites_management import switch_to_tds
from sqa_engine.utilities.executor.process_executor import (
    run_allure_report_server, create_latest_combined_log,
    execute_behave_test_cases
)
from sqa_engine.automation_libs.ui_automation.ui_based_class import \
    DESIRED_CAPABILITIES


def execute_test_cases(parameters: Namespace) -> None:
    """Execute test cases.

    This function gets arguments and calls module process_executor to run test
    cases.

    Parameters
    ----------
    parameters : argparse.Namespace
        All arguments that user specified.

    Returns
    -------
    None
    """
    test_module = parameters.module.lower()
    gen_scripts = parameters.gen_scripts
    debug = parameters.debug

    priority = DESIRED_CAPABILITIES.get(current_thread().name,  {}).get(
        "priority", parameters.priority
    )
    priorities = [priority_.lower() for priority_ in priority]

    test_designs = DESIRED_CAPABILITIES.get(current_thread().name, {}).get(
        "testDesigns", parameters.test_designs
    )

    tags = DESIRED_CAPABILITIES.get(current_thread().name, {}).get(
        "tags", parameters.tags
    )

    browser = parameters.browser

    generate_current_time_execution_log_dir(module_name=test_module)

    for test_design in test_designs:

        all_params = switch_to_tds(
            module=test_module, test_design=test_design, tags=tags,
            priorities=priorities, browser=browser,
            version=parameters.version.lower(), debug=debug
        )

        if not all_params:
            continue

        generate_test_suite_logs_folder(test_design=test_design)

        execute_behave_test_cases(
            list(all_params), debug=debug, gen_scripts=gen_scripts,
            stop_on_failure=parameters.stop_on_failure
        )

    # Export latest combined logs
    create_latest_combined_log()

    if parameters.run_allure and not gen_scripts:
        allure_local_session = run_allure_report_server()
        input("Press any key to end the Allure report session.")
        allure_local_session.terminate()
        allure_local_session.wait()
