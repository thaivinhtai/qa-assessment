#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module handles call to other processes.

    Functions in this module:

        +   create_empty_file(path_to_file: str) -> None
                Create empty file.

        +   copy_files(source_path: str, des_path: str) -> None
                Copy all items of a folder to other folder.

        +   execute_robot_test_cases(*args) -> None
                Execute robot test cases.

        +   generate_allure_report() -> None
                Generate allure report

        +   run_allure_report_server() -> None
                Open Allure report.

        +   run_appium_server() -> Popen
                Run Appium server.

        +   run_android_emulator(name: str) -> Popen
                Run Android emulator

        +   push_result() -> None
                Push execution results to report sever.
"""

from os import environ, listdir, path, mkdir, chdir

from subprocess import Popen
from time import sleep
from json import load, dumps, dump
from sys import stdout
from shutil import copyfile, rmtree, copy2, copytree

from io import StringIO

from datetime import datetime

import base64
import sys
import contextlib

from requests import Session

from behave.__main__ import main as behave_main
from behave.step_registry import registry

from sqa_engine.utilities.workspace_vars import InternalPath

from sqa_engine.general_config.tms_hosts import LINK_PATTERN, ISSUE_PATTERN


def print_progress_bar(wait_time: float, progress_status: str,
                       done: bool = False) -> None:
    """Print progress bar.

    This function visible the progress on console.

    Parameters
    ----------
    wait_time : float
        total time to wait.
    progress_status : str
        Status of the process.
    done : bool
        If done, progress bar shows 100%
    """

    def progress_bar(count: int, total: float, status: str):
        bar_len = 60
        filled_len = int(round(bar_len * count / float(total)))

        percents = round(100.0 * count / float(total), 1)
        bar = '=' * filled_len + '-' * (bar_len - filled_len)

        stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
        stdout.flush()

    increase = 0.01
    total_progress = wait_time / increase
    counter = 0
    while counter < total_progress:
        if done:
            counter = total_progress
        progress_bar(counter, total_progress, progress_status)
        sleep(increase)  # emulating long-playing job
        counter += 1


def create_empty_file(path_to_file: str) -> None:
    """Create an empty file.

    This function creates an empty file with specified path.

    Parameters
    ----------
    path_to_file : str
        Should be a full path to file.

    Returns
    -------
    None
    """
    open(path_to_file, 'w')


def copy_files(source_path: str, des_path: str, symlinks: bool = False,
               ignore=None) -> None:
    """Copy all file in source directory to an other directory.

    Parameters
    ----------
    source_path : str
        Path of source directory.
    des_path : str
        Path of des-directory.
    symlinks : bool
    ignore

    Returns
    -------
    None
    """
    for item in listdir(source_path):
        source = path.join(source_path, item)
        dest = path.join(des_path, item)
        if path.isdir(source):
            copytree(source, dest, symlinks, ignore)
        else:
            copy2(source, dest)


def trunc_datetime(some_date: datetime) -> datetime:
    """

    Parameters
    ----------
    some_date

    Returns
    -------

    """
    return some_date.replace(day=1, hour=0, minute=0, second=0, microsecond=0)


def get_json_object(file_path: str) -> dict:
    """

    Parameters
    ----------
    file_path

    Returns
    -------

    """
    result_file_object = open(file_path, "r", encoding="utf8")
    json_object = load(result_file_object)
    result_file_object.close()
    return json_object


def write_json(json_object: dict, file_path: str) -> None:
    """

    Parameters
    ----------
    json_object
    file_path

    Returns
    -------

    """
    result_file_object = open(file_path, "w")
    dump(json_object, result_file_object)
    result_file_object.close()


def get_all_current_allure_result_json_files() -> list:
    """

    Returns
    -------

    """
    allure_result_files = \
        listdir(InternalPath.current_allure_result_folder[-1])
    allure_result_json_files = [
        f'{InternalPath.current_allure_result_folder[-1]}/{file_name}'
        for file_name in allure_result_files
        if file_name.endswith('result.json')
    ]
    return allure_result_json_files


def count_all_test_cases_and_mark_with_not_yet_executed() -> None:
    """

    Returns
    -------

    """
    allure_result_json_files = get_all_current_allure_result_json_files()
    for result_file in allure_result_json_files:
        json_object = get_json_object(result_file)
        json_object["status"] = "skipped"
        write_json(json_object, result_file)


def execute_behave_test_cases(
        robot_params: list, debug: bool = False, gen_scripts: bool = False,
        stop_on_failure: bool = False) -> None:
    """Execute behave test cases.

    This function call the python behave and export log.

    Parameters
    ----------
    debug : bool
        True to debug.
    gen_scripts : bool
        Run test in dry-run mode.
    stop_on_failure : Immediately stop the test script.
    robot_params : list
        List of arguments to put into robot sub-process.

    Returns
    -------
    None
    """

    class TeeIO:
        def __init__(self, original, target):
            self.original = original
            self.target = target

        def write(self, b):
            open(f"{InternalPath.current_log_folder}/execution.log", "a+")\
                .write(b)
            self.original.write(b)
            self.target.write(b)

        def flush(self):
            self.original.flush()

    @contextlib.contextmanager
    def tee_stdout(target):
        tee = TeeIO(sys.stdout, target)
        with contextlib.redirect_stdout(tee):
            yield

    feature_file_name = robot_params[0].split('/')[-1]
    feature_name = feature_file_name.replace(".feature", "")
    feature_dir = robot_params[0].replace(feature_file_name, '')
    feature_file = robot_params[0]

    log_level = "INFO"
    if debug and not gen_scripts:
        log_level = "DEBUG"

    dry_run = ""
    pretty_run_log = "--format=pretty --summary"
    if gen_scripts:
        document_folder = InternalPath.current_doc_folder
        while '\\' in document_folder:
            document_folder = document_folder.replace('\\', '/')
        dry_run = f"--dry-run --format sphinx.steps --outfile " \
                  f"{document_folder}/{feature_name}.rst "
        pretty_run_log = ""

    stop = ""
    if stop_on_failure:
        stop = "--stop"

    while '\\' in feature_file:
        feature_file = feature_file.replace('\\', '/')
    params = robot_params[1]

    test_cases = ""
    for test_name in params.get("test", []):
        if test_name.find("--") > 0:
            test_name = test_name[:test_name.find("--")]
        if test_name.find("<") > 0:
            test_name = test_name[:test_name.find("<")]
        test_cases += f" --name '{test_name}'"

    variables = ""
    try:
        for variable in params.get("variable", []):
            name_and_value = variable.split(":")
            var_name = name_and_value[0]
            value_ = name_and_value[1]
            variables += f" --define {var_name}={value_}"
    except TypeError:
        pass

    tags = ""
    for tag in params.get("include", []):
        tags += f" --tags @{tag}"

    output_result_dir = InternalPath.current_allure_result_folder[-1]
    while '\\' in output_result_dir:
        output_result_dir = output_result_dir.replace("\\", '/')

    junit_report_dir = InternalPath.current_log_folder
    while '\\' in junit_report_dir:
        junit_report_dir = junit_report_dir.replace("\\", '/')

    chdir(feature_dir)
    buf = StringIO()
    with tee_stdout(buf):
        behave_main(f"--logging-level {log_level} --show-timings "
                    f"--format allure_behave.formatter:AllureFormatter "
                    f"--junit {pretty_run_log} --summary --no-logcapture -D "
                    "AllureFormatter.issue_pattern="
                    f"{ISSUE_PATTERN} -D "
                    "AllureFormatter.link_pattern="
                    F"{LINK_PATTERN} "
                    f'-o "{output_result_dir}" --junit-directory '
                    f'"{junit_report_dir}" {stop} {dry_run} {tags} {variables}'
                    f" {test_cases} {feature_file}")
    del buf
    registry.steps = {"given": [], "when": [], "then": [], "step": []}

    copyfile(
        InternalPath.allure_categories,
        f"{InternalPath.current_allure_result_folder[-1]}/categories.json"
    )
    copyfile(
        InternalPath.allure_environment,
        f"{InternalPath.current_allure_result_folder[-1]}/"
        f"environment.properties"
    )


def _get_all_result_files_in_current_execution() -> tuple:
    """Get all result files in current execution.

    This function gets all files name and their absolute path of Allure result
    in current execution time.

    Returns
    -------
    tuple
        (result_files - list of absolute path to files,
         files - list name of files)
    """
    result_dir = InternalPath.current_allure_result_folder

    result_files = list()
    files = list()
    meta_data = ['categories.json', 'environment.properties']
    for directory in result_dir:
        files += [file for file in listdir(directory)
                  if file not in meta_data]
        result_files += [f'{directory}/{file}' for file in listdir(directory)
                         if file not in meta_data]

    files += meta_data

    result_files += [InternalPath.allure_categories,
                     InternalPath.allure_environment]
    return result_files, files


def create_latest_combined_log() -> None:
    """Create latest combined log.

    Combine all output.xml from all current execution suite in to one xml.

    Returns
    -------
    None
    """
    latest_combined_log_path = \
        f"{InternalPath.logs_folder}/latest_combined_log"

    if not path.exists(latest_combined_log_path):
        mkdir(f"{InternalPath.logs_folder}/latest_combined_log")

    collect_current_allure_result_and_generate_report()


def collect_current_allure_result_and_generate_report() -> None:
    """Collect current Allure result and generate report.

    Collect all the Allure result, copy it to temp folder and then generate
    Allure report.

    Returns
    -------
    None
    """
    if not path.exists(f"{InternalPath.logs_folder}"
                       f"/latest_combined_log/allure"):
        mkdir(f"{InternalPath.logs_folder}/latest_combined_log/allure")

    temp_allure_result = (f"{InternalPath.logs_folder}"
                          f"/latest_combined_log/allure/result")
    temp_allure_report = (f"{InternalPath.logs_folder}"
                          f"/latest_combined_log/allure/report")

    if path.exists(temp_allure_result):
        rmtree(temp_allure_result)

    result_files, files = _get_all_result_files_in_current_execution()
    mkdir(temp_allure_result)

    for full_path, file_name in zip(result_files, files):
        copyfile(full_path, f"{temp_allure_result}/{file_name}")

    try:
        allure_local_generating = Popen(
            ['allure', 'generate', temp_allure_result,
             '--output', temp_allure_report, '--clean'],
            env=environ
        )
    except FileNotFoundError:
        allure_local_generating = Popen(
            [InternalPath.allure_bin, 'generate', temp_allure_result,
             '--output', temp_allure_report, '--clean'],
            env=environ
        )
    print_progress_bar(
        wait_time=3,
        progress_status=f"Generating Allure local report."
    )
    allure_local_generating.wait()


def run_allure_report_server() -> Popen:
    """Run Allure report server.

    This function run Allure server with the latest log information.

    Returns
    -------
    Popen
        allure session
    """
    temp_allure_report = (f"{InternalPath.logs_folder}"
                          f"/latest_combined_log/allure/report")

    allure_local_session = \
        Popen(
            [InternalPath.allure_bin, 'open', temp_allure_report], env=environ
        )
    print_progress_bar(
        wait_time=2,
        progress_status=f"Starting Allure local report."
    )
    return allure_local_session


def push_result(project_id='default') -> None:
    """Push result.

    This function pushes execution result to report sever.

    Returns
    -------
    None
    """

    def attach_result():
        nonlocal results
        nonlocal file
        result = dict()
        if path.isfile(file):
            file_obj = open(file, 'rb')
            content = file_obj.read()
            if content.strip():
                base64_content = base64.b64encode(content)
                result['file_name'] = files[index]
                result['content_base64'] = \
                    base64_content.decode('UTF-8')
                results += [result if result not in results else []]
            else:
                print(f'Empty file skipped: {file}')
            file_obj.close()
        else:
            print(f'Directory skipped: {file}')

    print("------------------------------------------------------------------")
    print(f"PUSH RESULT TO: {project_id}")
    print("------------------------------------------------------------------")

    headers = {'Content-type': 'application/json'}

    # Get server info
    data = open(InternalPath.report_server_info, 'r')
    server_meta_data = load(data)
    data.close()
    server_address = server_meta_data.get('server_address')
    send_results_endpoint = server_meta_data.get('send-results_api_endpoint')

    session = Session()

    # Upload file part
    results = list()
    index = 0

    result_files, files = _get_all_result_files_in_current_execution()
    for file in result_files:
        attach_result()
        index += 1

    # headers['X-CSRF-TOKEN'] = csrf_access_token
    request_body = {
        'results': results
    }
    json_request_body = dumps(request_body)

    response = \
        session.post(
            f'{server_address}{send_results_endpoint}?\
project_id={project_id}', headers=headers, data=json_request_body)

    print('STATUS CODE:')
    print(response.status_code)
    print('MASSAGE:')
    print(response.text)
    if str(response.status_code) == "400":
        print(json_request_body)
