#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions that relate to retrieving test suite.

    Private functions:

        +   __convert_path_to_module(path: str, module: str) -> str
                Converts a path and module name to Python's import form.

    Public function:

        +   convert_sequence_to_camel_case(snake_str: str) -> str
                Converts snake case to camel case.

        +   get_module_children(module_name: str) -> dict
                Get a dictionary that stores path of sequence file, robot file
                and other metadata of module.

        +   get_list_test_cases_and_robot_file(module_name: str,
                                               test_design: str) -> tuple:
                Get list test cases and path of robot file.
"""

from os import listdir, walk, name
from string import ascii_lowercase
from typing import Union, Iterable

from sqa_engine.utilities.workspace_vars.internal_path import InternalPath


def convert_sequence_to_camel_case(snake_str: str) -> str:
    """Convert sequence to camel case.

    This private function converts snake case to camel case and inserts
    statement 'Sequence' at the beginning.

    Parameters
    ----------
    snake_str : str
        A string in snake case form.

    Returns
    -------
    str
        'Sequence' + '{camel_case}'
    """
    components = snake_str.split('_')
    return 'Sequence' + ''.join(component.title() for component in components)


def __convert_path_to_module(path: str, module: str) -> str:
    """Convert path to module.

    This private function converts a path and module name to Python's import
    form.

    Parameters
    ----------
    path : str
        A path.
    module : str
        A name.

    Returns
    -------
    str
        a string with construct: '{parent}.{child}'
    """
    path = path[path.rfind(f'sqa_{module}'):len(path) - 3]
    components = path.split('/')
    return f'{components[0]}.'\
        + '.'.join(component for component in components[1:])


def __get_all_leaf_dirs(root_dir: str) -> list:
    """Get all leaf-dirs.

    This function gets all lowest level directories.

    Parameters
    ----------
    root_dir : str
        Path to root folder

    Returns
    -------
    list
        List of lowest level directories.
    """
    return [root for root, dirs, _ in walk(root_dir) if not dirs]


def get_module_children(module_name: str) -> dict:
    """Get module's children.

    This module bases on module_name to get a dictionary that stores path of
    sequence file, robot file and other metadata of module.

    Parameters
    ----------
    module_name : str
        Name of module to be tested [
            mobile_app, web_portal,
            rest_api, cooperation
        ]

    Returns
    -------
    dict
        A dictionary that stores path of sequence file, robot file, metadata.
    """
    return InternalPath.test_designs.get(module_name.lower())


def get_test_suite_path(test_module: str, test_design: str) -> tuple:
    """

    Parameters
    ----------
    test_module
    test_design

    Returns
    -------

    """
    packages = get_module_children(module_name=test_module)

    path_to_test_design = test_design.split('/', 1)
    test_design = path_to_test_design[0]
    destination_suite = path_to_test_design[-1].split('/')[-1]
    try:
        path_to_test_suite = f'{path_to_test_design[1]}/'
    except IndexError:
        path_to_test_suite = ""

    test_suite = f'{packages[test_design]}'
    return test_design, test_suite, path_to_test_suite, destination_suite


def get_list_test_cases_and_feature_file(module_name: str,
                                         test_design: str) -> tuple:
    """Get list test cases and path of feature file.

    This function get list of test cases via calling method of sequence class
    of test domain. Also, return path to robot file that contains test suite
    as well.

    Parameters
    ----------
    module_name : str
        Name of test module {mobile_app, web_portal, rest_api}.
    test_design : str
        Test domain specification.

    Returns
    -------
    tuple
        list : list of test cases.
        str : path to robot file that contains test suite.
    """
    packages = get_module_children(module_name=module_name)

    test_design, test_suite, path_to_test_suite, destination_suite = \
        get_test_suite_path(test_module=module_name, test_design=test_design)

    test_suite_to_be_executed = {
        'sequence_file': f'{packages[test_design]}/\
{path_to_test_suite}sequence_{destination_suite}.py',
        'feature_file': f'{packages[test_design]}/\
{path_to_test_suite}{destination_suite}.feature'
    }

    sequence_file = test_suite_to_be_executed['sequence_file']
    feature_file = test_suite_to_be_executed['feature_file']

    sequence_module = __convert_path_to_module(path=sequence_file,
                                               module=module_name)
    sequence_class = convert_sequence_to_camel_case(test_design)
    __import_module = __import__(sequence_module,
                                 globals(), locals(), [sequence_class], 0)

    list_test_cases = __import_module.get_list_test_cases()

    return list_test_cases, feature_file


def get_all_test_suite_in_module(module_name: str) -> list:
    """Get all test suite in module.

    This function gets all test suites name in module.

    Parameters
    ----------
    module_name : str
        Name of test module {mobile_app, web_portal, rest_api}.

    Returns
    -------
    list
        List of name of all test suites.
    """
    packages = get_module_children(module_name=module_name)
    all_files = listdir(packages['test_design'])
    temp = list()
    for file in all_files:
        if file.lower()[0] not in ascii_lowercase:
            temp.append(file)
    for file_to_remove in temp:
        all_files.remove(file_to_remove)
    return all_files


def get_list_test_suite(
        module_name: str, list_test_suites: Union[list, Iterable]) -> list:
    """Get all test suite in module.

    This function gets all test suites name in module.

    Parameters
    ----------
    module_name : str
        Name of test module {mobile_app, web_portal, rest_api, cooperation}.
    list_test_suites : list
        list of test suites.

    Returns
    -------
    list
        List of name test suites.
    """
    final_list_test_suites = list()

    if list_test_suites[0].lower() != 'all' and len(list_test_suites) >= 1:
        return list_test_suites

    root_dir = InternalPath.test_design_path.get(module_name)
    if name == "nt":
        root_dir += "\\"
    else:
        root_dir += "/"
    for directory in __get_all_leaf_dirs(root_dir):
        directory = directory[:-6] if directory[-6:] == "/steps" else directory
        suite = directory.replace(f'{root_dir}', '')
        final_list_test_suites.append(suite.replace('\\', '/'))

    # Clear __pycache__
    try:
        final_list_test_suites.remove('__pycache__')
    except ValueError:
        pass
    for index, suite in enumerate(final_list_test_suites):
        if '__pycache__' in suite:
            final_list_test_suites[index] = \
                final_list_test_suites[index].replace('/__pycache__', '')

    return final_list_test_suites
