#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module handles test cases metadata.

    Functions in this module:

        +   get_test_metadata(name: str, tags: list) -> str
                Get test case metadata via tags.

        +   get_tags(robot_suite: str) -> tuple
                Get tags of test suite and tags of each test cases
"""

from gherkin.stream.gherkin_events import GherkinEvents
from gherkin.stream.source_events import SourceEvents


class TestCaseModel:
    """Test case model.

    A test case model instance should contain: name, documentation, tags, and
    other meta data if needed.
    """
    def __init__(self, name: str, documentation: str, tags: tuple, **kwargs):
        """Constructor.

        Parameters
        ----------
        name : str
            test case name
        documentation : str
            documentation of test case
        tags : tuple
            list tags of test case
        kwargs : dict
            other meta data (key=value)
        """
        self.name = name
        self.documentation = documentation
        self.tags = tags
        self.__dict__.update(kwargs)


def get_test_metadata(test_name: str, name: str, tags: tuple) -> str:
    """Get test case metadata.

    This function get test case metadata via tags of that test case.

    Parameters
    ----------
    test_name : str
        Name of test case.
    name : str
        Name of metadata.
    tags : tuple
        List contains all tags of test case.

    Returns
    -------
    str
        Value of metadata.
    """
    prefix = name.lower() + '='
    for tag in tags:
        if tag.lower().startswith(prefix):
            return tag[len(prefix):]
    raise ValueError(f"Metadata {name} not found in test case {test_name}!")


def get_behave_testcase_documentation_and_tags(feature_file: str) -> dict:
    """Get tags.

    This function get feature file as argument
    and return a nested dictionary of:
        + a list of all tags in test suite
        + a dictionary of pairs "test case name": list of tags.

    Parameters
    ----------
    feature_file : str
        Path to feature file.

    Returns
    -------
    dict
        {test_case_name: [tag1, tag2, ...]}
    """

    def get_test_metadata_in_document(documentation: str) -> list:
        """Get test metadata in documentation.

        Split the documentation into list of lines and collect important
        metadata.

        Parameters
        ----------
        documentation : str

        Returns
        -------
        list
            [level, min_version, ...]
        """
        list_doc = documentation.split("\n")
        while "" in list_doc:
            list_doc.remove("")
        for line in range(len(list_doc)):
            list_doc[line] = list_doc[line].strip().rstrip()
        list_metadata = [
            item.lower() for item in list_doc if
            item.lower().startswith("priority") or
            item.lower().startswith("min_version") or
            item.lower().startswith("max_version") or
            item.lower().startswith("mode")
        ]
        return list_metadata

    class Options:
        print_source = False
        print_ast = True
        print_pickles = False

    args = [feature_file]

    source_events = SourceEvents(args)
    gherkin_events = GherkinEvents(Options)

    source_event = [source_event for source_event in source_events.enum()][0]
    event = [event for event in gherkin_events.enum(source_event)][0]

    test_case_tags_mapping = {}
    for scenario in event.get("gherkinDocument").get("feature").get(
            "children"):
        test_case_tags_mapping[scenario.get("scenario").get("name")] = []
        for tag in scenario.get("scenario").get("tags"):
            test_case_tags_mapping[
                scenario.get("scenario").get("name")
            ].append(tag.get("name"))
        test_case_tags_mapping[
            scenario.get("scenario").get("name")
        ] += get_test_metadata_in_document(
            scenario.get("scenario").get("description")
        )

    return test_case_tags_mapping
