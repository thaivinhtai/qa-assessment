#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to setup environ variables for the project.

        Function in this module:

            +   set_version_and_environment_env_var() -> None
                    Setup value for PYTHONPATH

            +   announce_debug_is_enable() -> None
                    Set value for ANDROID_HOME
"""

from os import environ


def set_version_and_environment_env_var(version: str,
                                        environment: str) -> None:
    """

    Parameters
    ----------
    version
    environment

    Returns
    -------

    """
    environ['VERSION'] = version
    environ['ENVIRONMENT'] = environment


def announce_debug_is_enable(debug=None) -> None:
    """

    Parameters
    ----------
    debug

    Returns
    -------

    """
    environ['FRAMEWORK_DEBUGGING'] = str(debug)
