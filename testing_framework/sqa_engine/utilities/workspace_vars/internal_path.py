#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module is for storing path of project.
"""

from pathlib import Path
from os import listdir, path, name


def get_children(module: str) -> dict:
    """Get children files.

    This private function gets name of module and collects all files of a
    module based on its name.

    Parameters
    ----------
    module : str
        Name of testing module

    Returns
    -------
    dict
        A dictionary of module and its file.
    """
    result_dict = dict()
    try:
        children = listdir(module)
    except FileNotFoundError:
        return result_dict
    for directory in children:
        # Ignore files and __pycache__ directory.
        if path.isfile(directory) or directory.find('__') == 0:
            continue
        result_dict[directory] = \
            f'{module}/{directory}'
    return result_dict


class InternalPath:
    """Internal path container.

    This class stores all the path of every packages, files that will be used
    in execution progressing.

    Class constants
    ----------
    workspace_path : str
        Absolute path of workspace folder.
    logs_folder : str
        Absolute path of root log folder.
    document_folder : str
        Absolute path of root document folder.
    __sqa_package : dict
        A dictionary of absolute path of packages in workspace (project).
    __mobile_app : dict
        A dictionary of test domain and their absolute path of mobile app.
    __allure_bin : str
        Absolute path of Allure executable bin file.
    __current_allure_report_folder : str
        Absolute path of folder that stores Allure report files.
    __current_robot_report_folder : str
        Absolute path of folder that stores Robot report files.
    __current_log_folder : str
        Absolute path of root folder that stores log files.
    __current_doc_folder : str
        Absolute path of root folder that stores documentation.
    __allure_job_result_folder : str
        Absolute path of folder that stores result allure for report sever
    __report_server_info : str
        Absolute path of report_server_info.json

    Methods
    -------
    __get_path(self, module: str) -> dict
        A private method to return all the files (files and folders are
        included) in a module.
    """

    workspace_path = str(Path(__file__).parent.absolute().parent.parent.parent)
    logs_folder = f'{workspace_path}/logs'
    document_folder = f'{workspace_path}/documentations'

    sqa_engine_path = f"{workspace_path}/sqa_engine"
    report_server_info = \
        f'{sqa_engine_path}/allure_config/report_server_info.json'

    allure_categories = f'{sqa_engine_path}/allure_config/categories.json'
    allure_environment = \
        f'{sqa_engine_path}/allure_config/environment.properties'

    sqa_web_path = f"{workspace_path}/sqa_web"
    sqa_api_path = f"{workspace_path}/sqa_api"

    test_module_path = {
        "web": sqa_web_path,
        "api": sqa_api_path
    }

    test_modules = {
        "web": get_children(sqa_web_path),
        "api": get_children(sqa_api_path)
    }

    test_design_path = {
        "web": f"{sqa_web_path}/test_designs",
    }

    test_designs = {
        "web": get_children(test_modules.get("web").get("test_designs"))
    }

    allure_bin = f'{sqa_engine_path}/tools/allure/bin/allure'
    if name == "nt":
        allure_bin += ".bat"

    chrome_driver_dir = f'{sqa_engine_path}/tools/web_drivers'
    chrome_driver_mapping_file = f'{chrome_driver_dir}/chrome_mapping_version'

    current_allure_result_folder = list()
    current_output_xml = list()
    urrent_allure_report_folder = str()
    current_robot_report_folder = str()
    current_screenshot_folder = str()
    current_log_folder = str()
    current_syslog_robot_path = str()
    current_time_execution_log_dir = str()
    current_doc_folder = str()
