#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Package workspace_vars contains modules that relate to environ variables of
project.
"""

from .internal_path import InternalPath
from .env_vars_setup import\
    set_version_and_environment_env_var, announce_debug_is_enable
