#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module stores the test data.
"""

NEW_USER_FULL_INFO = {
    "firstName": "Tai",
    "lastName": "Thai",
    "title": "QA Engineer",
    "company": "Foo",
    "phoneNumber": "+84 969 933 710",
    "website": "https://foo.bar",
    "email": "tai.thai@foo.bar"
}

NEW_USER_REQUIRED_INFO = {
    "firstName": "Talon",
    "lastName": "Thai",
    "title": "QA Engineer",
    "company": "Foo",
    "email": "talon.thai@foo.bar"
}

DEFAULT_USER = {
    "id": 1,
    "firstName": "Lenox",
    "lastName": "Lutfi",
    "title": "QA Engineer",
    "company": "Contour",
    "phoneNumber": "123-456-7890",
    "website": "https://contour.network",
    "email": "lenox.lutfi@contour.network"
}
