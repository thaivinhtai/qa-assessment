#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains list of all test case in
RegisterAndLogin Test Design Specification.
"""


class SequenceProfileManager:
    """
    This class have one property that is all names of test case in TDS
    RegisterAndLogin.
    """

    def __init__(self):
        """Constructor."""
        self.__list_test_cases = list()
        self.__list_test_cases.append('WEB_ProfMgr_Func_01 - Verify the list of members on the homepage -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_02 - Verify adding a new member with full data -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_03 - Verify adding a new member with the minimal required data -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_04 - Verify adding a new member with only a space in the <field> -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_05 - Verify adding a new member with empty data in the <field> -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_06 - Verify adding a new member with invalid data in the <field> -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_07 - Verify adding a new member without checking on the agree-checkbox -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_08 - Verify closing the success message popup after adding a new member successfully -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_09 - Verify searching members using an existing query -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_10 - Verify searching members using a non-existing query -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_11 - Verify searching members without input the query -- <browser>')
        self.__list_test_cases.append('WEB_ProfMgr_Func_12 - Verify viewing a member info using a non-existing ID -- <browser>')

    @property
    def list_test_cases(self):
        return self.__list_test_cases


def get_list_test_cases() -> list:
    """Get list test cases.

    Returns
    -------
    list
        list of all names of test cases in TDS Absolute Alarming
    """
    return SequenceProfileManager().list_test_cases
