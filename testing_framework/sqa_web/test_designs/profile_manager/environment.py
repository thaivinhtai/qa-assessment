#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

from sqa_engine.automation_libs.common_funcs import \
    set_default_test_case_behavior, retry_on_failure,\
    mapping_browser_name_with_scenario_outline, \
    set_story_suite_for_desktop_web
from sqa_web.libs.page_objects_lib.home_page import HomePage


def before_feature(context, feature):
    for scenario_ in feature.scenarios:
        scenario_ = mapping_browser_name_with_scenario_outline(
            scenario_, HomePage().driver.browser)
        retry_on_failure(scenario_)


def before_scenario(context, scenario):
    context.name = scenario.name
    set_default_test_case_behavior(scenario=scenario,
                                   tsm_link_mapping=__TMS_LINK_MAPPING)
    set_story_suite_for_desktop_web(
        browser_name=HomePage().driver.browser, suite=context.feature.name
    )
    context.headless = HomePage().driver.headless
    if HomePage().driver.driver:
        if len(HomePage().driver.driver.window_handles) != 1:
            HomePage().driver.close_session()


def after_scenario(context, scenario):
    if len(HomePage().driver.driver.window_handles) > 1:
        HomePage().driver.close_browser()
        HomePage().driver.close_session()


def after_feature(context, feature):
    HomePage().driver.close_browser()
    HomePage().driver.close_session()


__TMS_LINK_MAPPING = {
    'WEB_ProfMgr_Func_01':
        'WEB_ProfMgr_Func_01-9fb1e37257514e79b0586773a4c4dcfd',
    'WEB_ProfMgr_Func_02':
        'WEB_ProfMgr_Func_02-b30c16134b8140faa6fe2721bab24626',
    'WEB_ProfMgr_Func_03':
        'WEB_ProfMgr_Func_03-741276e0d2d944da8bc9dd05fdea9da2',
    'WEB_ProfMgr_Func_04':
        'WEB_ProfMgr_Func_04-e716f67a18394b7589382a8da324d9d1',
    'WEB_ProfMgr_Func_05':
        'WEB_ProfMgr_Func_05-68d86deb87a3427d937f61ae22758fa3',
    'WEB_ProfMgr_Func_06':
        'WEB_ProfMgr_Func_06-2301d29aed144145ae0fb294778d0029',
    'WEB_ProfMgr_Func_07':
        'WEB_ProfMgr_Func_07-d65324a7d84f4378990caa8945697acc',
    'WEB_ProfMgr_Func_08':
        'WEB_ProfMgr_Func_08-541e6d94f75344cdbb824c5a0d948774',
    'WEB_ProfMgr_Func_09':
        'WEB_ProfMgr_Func_09-91b1b7a2d051479e8b8304f5f38d8801',
    'WEB_ProfMgr_Func_10':
        'WEB_ProfMgr_Func_10-ef2f50f3452748148bde6bca7d0aebb5',
    'WEB_ProfMgr_Func_11':
        'WEB_ProfMgr_Func_11-cf02fa5c70134c76bd63f23427183938',
    'WEB_ProfMgr_Func_12':
        'WEB_ProfMgr_Func_12-7f6962b501544bf68c81ce4e2c7d092c',
}
