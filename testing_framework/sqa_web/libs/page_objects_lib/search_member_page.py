#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""


from .home_page import HomePage


class SearchMember(HomePage):
    def __init__(self):
        super().__init__()
        self.query_textbox = 'xpath=//input[@type="search"]'
        self.search_button = 'xpath=//button[@type="submit"]'

    def search(self, query_string: str):
        self.driver.logger.info(f"Search with pattern {query_string}")
        self._send_string_to_locator(locator=self.query_textbox,
                                     str_to_be_sent=query_string)
        self._click_on_locator(self.search_button)
        return self.get_list_members()
