#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

"""

from sqa_engine.automation_libs.ui_automation.web_auto_lib import \
    WebFundamentalAction, WEB_DRIVER

from threading import current_thread


WEB_DRIVER[current_thread().name] = WebFundamentalAction()
