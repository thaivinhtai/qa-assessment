# Tai Thai - Automation QA


## The Automation Testing Framework

The detail document can be found [here](testing_framework/README.md).


## Getting started

Make sure your machine has already been installed with [Git](https://git-scm.com/).

Use the following commands to clone the project:

```
git clone https://gitlab.com/thaivinhtai/qa-assessment.git
```

## Configure the testing environments

The following steps were tested on macOS Ventura 13.0. However, those might run well on Debian-based distros.

1. Pre-re:
   - [Docker](https://www.docker.com/)
2. Deploy the needed services:
    ```
    cd qa-assessment
    source app/start_all
    ```
 
**In case you don't want to start the testing FE/BE, you still need to start the Allure report services. You can use docker-compose to start them as follows:**

```yaml
version: "3.9"

services:
  allure_api:
    image: "frankescobar/allure-docker-service"
    container_name: allure_api
    environment:
      CHECK_RESULTS_EVERY_SECONDS: 1
      KEEP_HISTORY: 1
      KEEP_HISTORY_LATEST: 25
    ports:
      - "5050:5050"
    volumes:
      - ${PWD}/projects:/app/projects

  allure_ui:
    image: "frankescobar/allure-docker-service-ui"
    environment:
      ALLURE_DOCKER_PUBLIC_API_URL: "http://localhost:5050"
      ALLURE_DOCKER_PUBLIC_API_URL_PREFIX: ""
    ports:
      - "5252:5252"
```

You can find the above configuration in the file [app/docker-compose.yml](./app/docker-compose.yml)

### Clean up the testing environments

To clean up the environments, execute the following commands:

```shell
source app/clean_up
```
